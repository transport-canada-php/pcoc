<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC;

use SoapFault;
use SoapClient;
use Exception;
use TransportCanada\PCOC\Enum\ServiceMessageCodes as Codes;
use TransportCanada\PCOC\Exception\AuthenticationException;
use TransportCanada\PCOC\Exception\ConnectionException;
use TransportCanada\PCOC\Exception\SAIKAuthenticationException;
use TransportCanada\PCOC\Exception\SystemException;
use TransportCanada\PCOC\Exception\DomainAuthenticationException;
use TransportCanada\PCOC\Exception\ValidationException;
use TransportCanada\PCOC\Exception\WebServiceAuthenticationException;

/**
 * PHP interface to Transport Canada's Pleasure Craft Operator Competency
 * Database System.
 */
class CompetencyDatabaseSystem
{
    /**
     * Key for accessing the last exception.
     *
     * @var string
     */
    const LAST_EXCEPTION = 'exception';

    /**
     * Key for accessing the last request.
     *
     * @var string
     */
    const LAST_REQUEST = 'request';

    /**
     * Key for accessing the last response.
     *
     * @var string
     */
    const LAST_RESPONSE = 'response';

    /**
     * Live Transport Canada API.
     *
     * @var string
     */
    const WSDL_LIVE = 'https://wwwapps.tc.gc.ca/Saf-Sec-Sur/4/PCOCWS-SWCCEP/Service.asmx';

    /**
     * Stage Transport Canada API.
     *
     * @var string
     */
    const WSDL_STAGE = 'https://kangawwwapps.tc.gc.ca/Saf-Sec-Sur/4/PCOCWS-SWCCEP/Service.asmx';

    /**
     * Maps response structures to classes.
     *
     * @var array
     */
    protected static $classmap = [
        'ArrayOfServiceMessageObj' => Schema\Messages::class,
        'AuthorizationResponseObj' => Schema\Exam::class,
        'AuthorizeResponse' => Response\Exam::class,
        'BaseResponseObj' => Schema\Response::class,
        'FinalTestResultResponseObj' => Schema\Grade::class,
        'GetFinalTestResultResponse' => Response\Grade::class,
        'ServiceMessageObj' => Schema\Message::class,
    ];

    /**
     * API connection.
     *
     * @var self
     */
    protected static $instance;

    /**
     * Authentication for API calls.
     *
     * @var \TransportCanada\PCOC\Request\Auth
     */
    protected $auth;

    /**
     * Authentication for Kanga API.
     *
     * @var \TransportCanada\PCOC\Request\Auth
     */
    protected $debug;

    /**
     * Configuration for connecting to the API.
     *
     * @var array
     */
    protected $config;

    /**
     * Errors gathered when our SoapClient fails to initialize.
     *
     * @var array
     */
    protected $errors = [];

    /**
     * Log for the last request, response, and exception.
     *
     * @var array
     */
    protected $last = [
        self::LAST_REQUEST => null,
        self::LAST_RESPONSE => null,
        self::LAST_EXCEPTION => null,
    ];

    /**
     * SoapClient used to call API.
     *
     * @var \SoapClient
     */
    protected $soap;

    /**
     * Disables the new constructor.
     *
     * @param array $config Connection configuration
     */
    private function __construct(array $config)
    {
        $this->config = $config;

        // Always prefer debug credentials!
        if (isset($config['debug'])) {
            $this->debug = new Request\Auth($config['debug']);
        } else {
            $this->auth = new Request\Auth($config['auth']);
        }
    }

    /**
     * Accessor to raw SoapClient instance.
     *
     * @return \SoapClient
     */
    public function soap()
    {
        return $this->soap;
    }

    /**
     * Attempts to connect to Transport Canada's API.
     *
     * @param array $config  Connection configuration
     * @param bool  $connect Auto-connect to API
     *
     * @throws \TransportCanada\PCOC\Exception\AuthenticationException
     * @throws \TransportCanada\PCOC\Exception\SystemException
     *
     * @return self
     */
    public static function configure(array $config, bool $connect = true)
    {
        $db = new static($config);

        if ($connect) {
            $db->connect();
        }

        return $db;
    }

    /**
     * Attempts to connect to TC's SOAP API.
     *
     * @throws \TransportCanada\PCOC\Exception\AuthenticationException
     * @throws \TransportCanada\PCOC\Exception\SystemException
     */
    public function connect()
    {
        if (null !== $this->soap) {
            return;
        }

        // Basic (HTTP) authentication needed to access the API
        $domain = $this->config['domain'];

        // Select Kanga if we're in debug mode
        $wsdl = null !== $this->debug ? self::WSDL_STAGE : self::WSDL_LIVE;

        // Transport Canada's SSL certificate doesn't work with native PHP SOAP
        // and even attempts with their certs have not succeeded in PHP so we
        // need to disable SSL verification to prevent WSDL parsing exceptions
        $soap = [
            'login' => $domain['username'] ?? '',
            'password' => $domain['password'] ?? '',
            'classmap' => static::$classmap,
            'exceptions' => true,
            // 'trace' => true,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'soap_version' => SOAP_1_2,
            'stream_context' => stream_context_create([
                'ssl' => [
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true,
                ],
            ]),
        ];

        // Override error handler to parse SOAP connection issues
        set_error_handler(function (...$args) {
            $this->handleError(...$args);
        });

        try {
            // Initializes the SOAP connection to Transport Canada's API
            $this->soap = new SoapClient($wsdl.'?wsdl', $soap);
        } catch (SoapFault $e) {
            // Safely handles fatal errors(i.e. HTTP 401)
            $this->last[self::LAST_EXCEPTION] = $this->handleSoapFault($e);
        } catch (Exception $e) {
            // Something else failed
            $this->last[self::LAST_EXCEPTION] = $this->handleException($e);
        }

        // Restore original error handler
        restore_error_handler();

        // Now we can throw the SOAP exceptions
        $this->rethrow();
    }

    /**
     * Handles returning or throwing failed requests.
     *
     * @param \Exception|null $ex    Exception to stash and possibly throw
     * @param bool            $force Forces throwing instead of returning false
     *
     * @return bool
     */
    protected function rethrow(Exception $ex = null, bool $force = false)
    {
        if (null === $ex) {
            $ex = $this->last[self::LAST_EXCEPTION];
        } else {
            $this->last[self::LAST_EXCEPTION] = $ex;
        }

        $throwable = null !== $ex;
        $throw = $this->config['exceptions'] ?? false;

        if ($throwable && ($throw || $force)) {
            throw $this->last[self::LAST_EXCEPTION];
        }

        return false;
    }

    /**
     * Retrieves the last request, response, and/or exception.
     *
     * @param string|null $key Last message key to fetch
     *
     * @return mixed
     */
    public function last(string $key = null)
    {
        return null !== $key ? $this->last[$key] : $this->last;
    }

    /**
     * Handles calling the API and returning the response schemas.
     *
     * Will return false if there was an error in calling the SOAP method.
     *
     * @param \TransportCanada\PCOC\Request\Request $request API call request
     * @param bool                                  $connect Auto-connect to TC API
     *
     * @throws \TransportCanada\PCOC\Exception\ConnectionException
     * @throws \TransportCanada\PCOC\Exception\AuthenticationException
     * @throws \TransportCanada\PCOC\Exception\DomainAuthenticationException
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     * @throws \TransportCanada\PCOC\Exception\SystemException
     *
     * @return \TransportCanada\PCOC\Response\Response|false
     */
    public function request(Request\Request $request, bool $connect = true)
    {
        $this->last = [
            self::LAST_REQUEST => $request,
            self::LAST_RESPONSE => null,
            self::LAST_EXCEPTION => null,
        ];

        if ($connect) {
            $this->connect();
        }

        if (null === $this->soap) {
            $this->rethrow($this->last[self::LAST_EXCEPTION] ?? new ConnectionException(), true);
        }

        try {
            // Can throw validation exceptions
            $request->auth($this->debug ?? $this->auth)->validate();

            // Prepare request
            $method = $request->method();
            $param = $request->decorate();

            /** @var \TransportCanada\PCOC\Response\Exam $response */ // phpcs:ignore
            /** @var \TransportCanada\PCOC\Response\Grade $response */ // phpcs:ignore
            // Send SOAP function(e.g. Authorize())
            $response = $this->last[self::LAST_RESPONSE] = $this->soap->{$method}($param);

            // Parse response to better handle failed SAIK authentication
            if ($response->hasCode(Codes::TC0000) || $response->hasCode(Codes::TC0001)) {
                throw new SAIKAuthenticationException();
            }

            // Handle failed web service authentication
            if ($response->hasCode(Codes::TC0014)) {
                throw new WebServiceAuthenticationException();
            }

            return $response;
        } catch (AuthenticationException $ex) {
            // User entered invalid credentials somewhere
            $this->last[self::LAST_EXCEPTION] = $ex;
        } catch (ValidationException $ex) {
            // User entered invalid request data
            $this->last[self::LAST_EXCEPTION] = $ex;
        } catch (SoapFault $ex) {
            // SoapClient threw an error - usually a HTTP 401
            $this->last[self::LAST_EXCEPTION] = $this->handleSoapFault($ex);
        } catch (Exception $ex) {
            // PHP threw an error
            $this->last[self::LAST_EXCEPTION] = $this->handleException($ex);
        }

        // Returns false or throws the stored exception
        return $this->rethrow();
    }

    /**
     * PHP error handler.
     *
     * @param int    $errno PHP error number
     * @param string $error PHP error message
     */
    protected function handleError($errno, $error)
    {
        if (!stristr($error, 'SoapClient::SoapClient')) {
            trigger_error($error, \E_ERROR);
        } else {
            $this->errors[] = preg_match('/http(.*) 401/i', $error)
                ? new DomainAuthenticationException()
                : new SystemException($error);
        }
    }

    /**
     * SoapClient failed- primarily for HTTP 401.
     *
     * @param \SoapFault $ex SoapFault to handle
     *
     * @return \TransportCanada\PCOC\Exception\DomainAuthenticationException|\TransportCanada\PCOC\Exception\SystemException
     */
    protected function handleSoapFault(SoapFault $ex)
    {
        // Checks to see if the fault was due to domain credentials
        $domain = Util\Util::find($this->errors, function ($error) {
            return $error instanceof DomainAuthenticationException;
        });

        // Always return domain errors then anything caught by set_error_handler
        return $domain ?? $this->errors[0] ?? $this->handleException($ex);
    }

    /**
     * SoapClient or PHP failed in an unknown way.
     *
     * @param \Exception $ex Exception to handle
     *
     * @return \TransportCanada\PCOC\Exception\SystemException
     */
    protected function handleException(Exception $ex)
    {
        return new SystemException($ex->getMessage(), $ex->getCode(), $ex);
    }
}
