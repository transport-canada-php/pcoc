<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Validate;

/**
 * Base request validation.
 *
 * @see \TransportCanada\PCOC\Schema\Struct\BaseRequestObj
 */
class Request
{
    /**
     * Character limit for usernames.
     *
     * @var int
     */
    public const USERNAME_MAX = 10;

    /**
     * Character limit for passwords.
     *
     * @var int
     */
    public const PASSWORD_MAX = 3000;

    /**
     * Validate authentication username(TC0014).
     *
     * @param string $value Username
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     */
    public function Username($value)
    {
        (new Rule\StringRule($value, __FUNCTION__))
            ->required()
            ->max(self::USERNAME_MAX);
    }

    /**
     * Validate authentication password(TC0014).
     *
     * @param string $value Password
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     */
    public function Password($value)
    {
        (new Rule\StringRule($value, __FUNCTION__))
            ->required()
            ->max(self::PASSWORD_MAX);
    }
}
