<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Validate\Rule;

use UnexpectedValueException;

/**
 * Email validation helper.
 */
class EmailRule extends StringRule
{
    /**
     * Override required rule to validate email address as well.
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     *
     * @return static
     */
    public function required()
    {
        parent::required();

        $this->email();

        return $this;
    }

    /**
     * Checks if the current value is a valid email address.
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     *
     * @return static
     */
    public function email()
    {
        // Using handy PHP validator instead of writing our own
        if (!filter_var($this->value, \FILTER_VALIDATE_EMAIL)) {
            $this->throw(new UnexpectedValueException($this->name.' isn\'t a valid email address'));
        }

        return $this;
    }
}
