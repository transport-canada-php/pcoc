<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Validate;

use TransportCanada\PCOC\Enum;

/**
 * Validator for operator information.
 *
 * @see \TransportCanada\PCOC\Schema\Struct\OperatorInformationObj
 */
class Operator
{
    /**
     * Character limit for first names.
     *
     * @var int
     */
    public const FIRST_NAME_MAX = 50;

    /**
     * Character limit for last names.
     *
     * @var int
     */
    public const LAST_NAME_MAX = 50;

    /**
     * Character limit for email addresses.
     *
     * @var int
     */
    public const EMAIL_MAX = 200;

    /**
     * Character limit for addresses.
     *
     * @var int
     */
    public const ADDRESS_MAX = 200;

    /**
     * Character limit for cities.
     *
     * @var int
     */
    public const CITY_MAX = 200;

    /**
     * Character limit for postal/zip codes.
     *
     * @var int
     */
    public const POSTAL_ZIP_CODE_MAX = 10;

    /**
     * Enum class for validating provinces.
     *
     * @var string
     */
    public const PROVINCE_CD_IN = Enum\Provinces::class;

    /**
     * Enum class for validating countries.
     *
     * @var string
     */
    public const COUNTRY_CODE_IN = Enum\Countries::class;

    /**
     * Enum class for validating genders.
     *
     * @var string
     */
    public const GENDER_CODE_IN = Enum\Gender::class;

    /**
     * Regex validation for birth dates.
     *
     * @var string
     */
    public const DATE_OF_BIRTH_REGEX = '/^\d{4}-\d{2}-\d{2}/';

    /**
     * Validates the operator's first name(TC0006).
     *
     * @param string $value FirstName
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     */
    public function FirstName($value)
    {
        (new Rule\StringRule($value, __FUNCTION__))
            ->required()
            ->max(self::FIRST_NAME_MAX);
    }

    /**
     * Validates the operator's last name(TC0008).
     *
     * @param string $value LastName
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     */
    public function LastName($value)
    {
        (new Rule\StringRule($value, __FUNCTION__))
            ->required()
            ->max(self::LAST_NAME_MAX);
    }

    /**
     * Validates the operator's email address(TC0005).
     *
     * @param string $value Email
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     */
    public function Email($value)
    {
        (new Rule\EmailRule($value, __FUNCTION__))
            ->required()
            ->max(self::EMAIL_MAX);
    }

    /**
     * Validates the operator's address line 1(TC0020).
     *
     * @param string $value Address
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     */
    public function Address($value)
    {
        (new Rule\StringRule($value, __FUNCTION__))
            ->required()
            ->max(self::ADDRESS_MAX);
    }

    /**
     * Validates the operator's city(TC0002).
     *
     * @param string $value City
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     */
    public function City($value)
    {
        (new Rule\StringRule($value, __FUNCTION__))
            ->required()
            ->max(self::CITY_MAX);
    }

    /**
     * Validates the operator's postal code(TC0010).
     *
     * @param string $value PostalZipCode
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     */
    public function PostalZipCode($value)
    {
        (new Rule\StringRule($value, __FUNCTION__))
            ->required()
            ->max(self::POSTAL_ZIP_CODE_MAX);
    }

    /**
     * Validates the operator's province code(TC0011).
     *
     * @param string $value ProviceCd
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     */
    public function ProvinceCd($value)
    {
        (new Rule\EnumRule($value, __FUNCTION__))
            ->required()
            ->in(\call_user_func([self::PROVINCE_CD_IN, 'enums']));
    }

    /**
     * Validates the operator's country code(TC0003).
     *
     * @param string $value CountryCode
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     */
    public function CountryCode($value)
    {
        (new Rule\EnumRule($value, __FUNCTION__))
            ->required()
            ->in(\call_user_func([self::COUNTRY_CODE_IN, 'enums']));
    }

    /**
     * Validates the operator's gender code(TC0007).
     *
     * @param string $value GenderCode
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     */
    public function GenderCode($value)
    {
        (new Rule\EnumRule($value, __FUNCTION__))
            ->required()
            ->in(\call_user_func([self::GENDER_CODE_IN, 'enums']));
    }

    /**
     * Validates the operator's date of birth(TC0004).
     *
     * @param string $value DateOfBirth
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     */
    public function DateOfBirth($value)
    {
        (new Rule\StringRule($value, __FUNCTION__))
            ->required()
            ->regex(self::DATE_OF_BIRTH_REGEX);
    }
}
