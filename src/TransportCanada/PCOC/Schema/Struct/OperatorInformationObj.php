<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Schema\Struct;

/**
 * Operator/candidate information object.
 */
class OperatorInformationObj
{
    /**
     * Street address of the candidate.
     *
     * @var string Max length: 200
     */
    public $Address = '';

    /**
     * City of the candidate.
     *
     * @var string Max length: 200
     */
    public $City = '';

    /**
     * Country of the candidate.
     *
     * @see \TransportCanada\PCOC\Enum\Countries
     *
     * @var string Country enum
     */
    public $CountryCode = '';

    /**
     * Date of birth of the candidate (use format YYYY-MM-DD).
     *
     * @var string
     */
    public $DateOfBirth = '';

    /**
     * Email address used to email the candidate their online test URL by the
     * Web Service on behalf of the course provider.
     *
     * @var string Max length: 200
     */
    public $Email = '';

    /**
     * Given name(s) of the candidate.
     *
     * @var string Max length: 50
     */
    public $FirstName = '';

    /**
     * Gender of the candidate.
     *
     * @see \TransportCanada\PCOC\Enum\Gender
     *
     * @var string Gender enum
     */
    public $GenderCode = '';

    /**
     * Surname of the candidate.
     *
     * @var string Max length: 50
     */
    public $LastName = '';

    /**
     * Undocumented and unused.
     *
     * @var string ?
     */
    public $MasterSaik = '';

    /**
     * Postal/zip code of the candidate.
     *
     * @var string Max length: 10
     */
    public $PostalZipCode = '';

    /**
     * Province/State Code of the candidate.
     *
     * @see \TransportCanada\PCOC\Enum\Provinces
     *
     * @var string Province enum
     */
    public $ProvinceCd = '';
}
