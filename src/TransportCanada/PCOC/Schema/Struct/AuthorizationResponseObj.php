<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Schema\Struct;

/**
 * Authorization response object.
 *
 * Note: Class used by the Web Service to communicate if the candidate is
 * authorized to take an online test and provides a URL and a list of message(s)
 * if applicable (if Authorized with Warnings).
 */
class AuthorizationResponseObj extends BaseResponseObj
{
    /**
     * The result status of the authorization request.
     *
     * @see \TransportCanada\PCOC\Enum\AuthorizationStatus
     *
     * @var string Status enum
     */
    public $AuthorizationStatus = '';

    /**
     * The URL returned to the online test.
     *
     * Includes the token that uniquely identifies the online test as a
     * parameter to the URL.
     *
     * The TestURL looks as follows for English:
     * https://wwwapps.tc.gc.ca/Saf-Sec-Sur/4/pcocdssbdccep/to/p1000.aspx?lang=0&token=,<token>
     *
     * The TestURL looks as follows for French:
     * https://wwwapps.tc.gc.ca/Saf-Sec-Sur/4/pcocdssbdccep/to/p1000.aspx?lang=1&token=,<token>
     *
     * @var string
     */
    public $TestUrl = '';

    /**
     * The date from which the online test identified by the TestURL is valid
     * is based on the current system date.
     *
     * @var string
     */
    public $TestUrlEffectiveDate = '';

    /**
     * The online test is valid for a prescribed amount of time, set at 30
     * days from the TestURLEffectiveDate.
     *
     * @var string
     */
    public $TestUrlExpiryDate = '';
}
