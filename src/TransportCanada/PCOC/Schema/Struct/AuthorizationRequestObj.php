<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Schema\Struct;

/**
 * Exam/authorization request object.
 */
class AuthorizationRequestObj extends BaseRequestObj
{
    /**
     * A data class with data provided by the course provider containing
     * candidate information (see 4.1.1.2).
     *
     * @var \TransportCanada\PCOC\Schema\Struct\OperatorInformationObj
     */
    public $OperatorInformation;

    /**
     * The unique identifier for a candidate specified by the course provider.
     *
     * This can be used to quickly identify the candidate when trying to
     * resolve any issues concerning them.
     *
     * @var string Max length: 50
     */
    public $OrganizationOperatorId = '';

    /**
     * The course provider URL (candidates will be redirected to this URL after
     * completing their online test).
     *
     * For example, if a course provider specifies ReturnUrl:
     * http://www.cp.web.site.com/ReturnURL.aspx?
     * then, upon completing the test on the Transport Canada site, a candidate
     * will be redirected to that URL with the following parameters appended:
     * http://www.cp.web.site.com/ReturnURL.aspx?token=<online test token>
     *
     * The token may be used by the course provider to obtain the candidate’s
     * final test results.
     *
     * @var string Max length: 500
     */
    public $ReturnUrl = '';

    /**
     * The SAIK provided to the course provider via PCOC Database System and
     * used to call the Web Service (see 3.3).
     *
     * @var string Max length: 200
     */
    public $SAIK = '';

    /**
     * The requested language of a candidate’s online test.
     *
     * @see \TransportCanada\PCOC\Enum\Language
     *
     * @var string Language enum
     */
    public $TestingLanguageCd = '';
}
