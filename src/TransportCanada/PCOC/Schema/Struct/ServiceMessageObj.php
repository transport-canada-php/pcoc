<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Schema\Struct;

/**
 * Service message object.
 *
 * This class contains a list of message codes that are returned when a call is
 * made to the Web Service. These codes can be used to determine if there
 * were issues with either the "authorise" request or the "getfinaltestresult"
 * request. The actual codes and message description can be found in 4.1.3.1.
 *
 * Note: Class used by the Web Service to identify issues with a request from
 * the course provider.
 */
class ServiceMessageObj
{
    /**
     * The code of the message (format TCnnnn).
     *
     * @var string
     */
    public $Code = '';

    /**
     * Description of the message that indicates the detail of the problem
     * with the request.
     *
     * @var string
     */
    public $Message = '';
}
