<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Request;

/**
 * Request interface.
 */
interface Request
{
    /**
     * Injects authentication data into our request needed for API calls.
     *
     * @param \TransportCanada\PCOC\Request\Auth $auth Authentication data
     *
     * @return self
     */
    public function auth(Auth $auth);

    /**
     * Validates the current request object.
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     *
     * @return bool
     */
    public function validate();

    /**
     * Decorate this request as another structure.
     *
     * @return object
     */
    public function decorate();

    /**
     * Returns the WSDL function name for the API.
     *
     * @return string
     */
    public function method();
}
