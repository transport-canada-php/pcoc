<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Response;

use TransportCanada\PCOC\Schema;
use TransportCanada\PCOC\Util;
use TransportCanada\PCOC\Enum\ResultStatus as Status;

/**
 * Wrapper for the API response when requesting a user's test results.
 */
class Grade extends Schema\Struct\GetFinalTestResultResponse implements Response
{
    use Helper\Has;
    use Util\Lenient;

    /**
     * Status for failing the exam.
     *
     * @var string
     */
    const FAIL = Status::FAIL;

    /**
     * Status for not completing the exam.
     *
     * @var string
     */
    const INCOMPLETE = Status::INCOMPLETE;

    /**
     * Status for not starting the exam.
     *
     * @var string
     */
    const NOT_STARTED = Status::NOT_STARTED;

    /**
     * Status for passing the exam.
     *
     * @var string
     */
    const PASS = Status::PASS;

    /**
     * Status for unknown exam state.
     *
     * @var string
     */
    const UNKNOWN = Status::UNKNOWN;

    /**
     * Handle dependency injection.
     *
     * @param \TransportCanada\PCOC\Schema\Grade $results Results struct
     */
    public function __construct(Schema\Grade $results)
    {
        $this->GetFinalTestResultResult = $results;
    }

    /**
     * Returns the number of questions the user passed.
     *
     * @return int
     */
    public function passed()
    {
        return (int) $this->results()->QuestionsPassed;
    }

    /**
     * Returns the total number of questions asked.
     *
     * @return int
     */
    public function questions()
    {
        return $this->results()->TotalQuestions;
    }

    /**
     * Shortcut function to get the test result object.
     *
     * @return \TransportCanada\PCOC\Schema\Grade
     */
    public function results()
    {
        return $this->GetFinalTestResultResult;
    }

    /**
     * Returns the percentage score.
     *
     * @return float
     */
    public function score()
    {
        return (float) $this->results()->ScorePercentage;
    }

    /**
     * Shortcut function to get the status from the test result object.
     *
     * @see \TransportCanada\PCOC\Enum\ResultStatus
     *
     * @return string
     */
    public function status()
    {
        return $this->results()->ResultStatus;
    }
}
