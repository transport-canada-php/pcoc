<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Response\Helper;

/**
 * Helper functions for handling response statuses.
 */
trait HasStatus
{
    /**
     * Checks if our response contains a specific status.
     *
     * @param string $status Status to check
     *
     * @see \TransportCanada\PCOC\Enum\AuthorizationStatus
     * @see \TransportCanada\PCOC\Enum\ResultStatus
     *
     * @return bool
     */
    public function hasStatus(string $status)
    {
        /* @var \TransportCanada\PCOC\Response\Response $this */
        return $this->status() === $status;
    }
}
