<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Response;

use TransportCanada\PCOC\Schema;
use TransportCanada\PCOC\Util;
use TransportCanada\PCOC\Enum\AuthorizationStatus as Status;

/**
 * Wrapper for the authorization response data.
 */
class Exam extends Schema\Struct\AuthorizeResponse implements Response
{
    use Helper\Has;
    use Util\Lenient;

    /**
     * Status for failed request.
     *
     * @var string
     */
    const FAIL = Status::FAIL;

    /**
     * Status for successful exam request.
     *
     * @var string
     */
    const OK = Status::OK;

    /**
     * Status for exam already completed.
     *
     * @var string
     */
    const PASSED = Status::PASSED;

    /**
     * Status for successful exam request but with some warnings.
     *
     * @var string
     */
    const WARNING = Status::OK_WARNING;

    /**
     * Handle dependency injection.
     *
     * @param \TransportCanada\PCOC\Schema\Exam $results Response struct
     */
    public function __construct(Schema\Exam $results)
    {
        $this->AuthorizeResult = $results;
    }

    /**
     * Returns the expiry date in epoch.
     *
     * @return int|null Null if the exam failed to fetch
     */
    public function end()
    {
        $date = $this->results()->TestUrlExpiryDate;
        $end = strtotime($date);

        // Failed status returns timestamps at 00-01-01 00:00:00
        return $end > 0 ? $end : null;
    }

    /**
     * Returns the current authorized response struct.
     *
     * @return \TransportCanada\PCOC\Schema\Exam
     */
    public function results()
    {
        return $this->AuthorizeResult;
    }

    /**
     * Returns the start time in epoch.
     *
     * @return int|null Null if exam failed to fetch
     */
    public function start()
    {
        $date = $this->results()->TestUrlEffectiveDate;
        $start = strtotime($date);

        // Failed status returns timestamps at 00-01-01 00:00:00
        return $start > 0 ? $start : null;
    }

    /**
     * Returns the associated exam status.
     *
     * @return string
     */
    public function status()
    {
        return $this->results()->AuthorizationStatus;
    }

    /**
     * Returns the associated exam URL from Transport Canada.
     *
     * @return string|null Null if exam failed to fetch
     */
    public function url()
    {
        return $this->results()->TestUrl;
    }
}
