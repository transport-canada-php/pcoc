<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Enum;

/**
 * Namespace for country enums.
 */
class Countries implements Enum
{
    /**
     * Afghanistan.
     *
     * @var string
     */
    const AF = 'AF';

    /**
     * Aland Islands.
     *
     * @var string
     */
    const AX = 'AX';

    /**
     * Albania.
     *
     * @var string
     */
    const AL = 'AL';

    /**
     * Algeria.
     *
     * @var string
     */
    const DZ = 'DZ';

    /**
     * American Samoa.
     *
     * @var string
     */
    const AS = 'AS';

    /**
     * Andorra.
     *
     * @var string
     */
    const AD = 'AD';

    /**
     * Angola.
     *
     * @var string
     */
    const AO = 'AO';

    /**
     * Anguilla.
     *
     * @var string
     */
    const AI = 'AI';

    /**
     * Antarctica.
     *
     * @var string
     */
    const AQ = 'AQ';

    /**
     * Antigua and Barbuda.
     *
     * @var string
     */
    const AG = 'AG';

    /**
     * Argentina.
     *
     * @var string
     */
    const AR = 'AR';

    /**
     * Armenia.
     *
     * @var string
     */
    const AM = 'AM';

    /**
     * Aruba.
     *
     * @var string
     */
    const AW = 'AW';

    /**
     * Australia.
     *
     * @var string
     */
    const AU = 'AU';

    /**
     * Austria.
     *
     * @var string
     */
    const AT = 'AT';

    /**
     * Azerbaijan.
     *
     * @var string
     */
    const AZ = 'AZ';

    /**
     * Bahamas.
     *
     * @var string
     */
    const BS = 'BS';

    /**
     * Bahrain.
     *
     * @var string
     */
    const BH = 'BH';

    /**
     * Bangladesh.
     *
     * @var string
     */
    const BD = 'BD';

    /**
     * Barbados.
     *
     * @var string
     */
    const BB = 'BB';

    /**
     * Belarus.
     *
     * @var string
     */
    const BY = 'BY';

    /**
     * Belgium.
     *
     * @var string
     */
    const BE = 'BE';

    /**
     * Belize.
     *
     * @var string
     */
    const BZ = 'BZ';

    /**
     * Benin.
     *
     * @var string
     */
    const BJ = 'BJ';

    /**
     * Bermuda.
     *
     * @var string
     */
    const BM = 'BM';

    /**
     * Bhutan.
     *
     * @var string
     */
    const BT = 'BT';

    /**
     * Bolivia.
     *
     * @var string
     */
    const BO = 'BO';

    /**
     * Bosnia and Herzegovina.
     *
     * @var string
     */
    const BA = 'BA';

    /**
     * Botswana.
     *
     * @var string
     */
    const BW = 'BW';

    /**
     * Bouvet Island.
     *
     * @var string
     */
    const BV = 'BV';

    /**
     * Brazil.
     *
     * @var string
     */
    const BR = 'BR';

    /**
     * British Virgin Islands.
     *
     * @var string
     */
    const VG = 'VG';

    /**
     * British Indian Ocean Territory.
     *
     * @var string
     */
    const IO = 'IO';

    /**
     * Brunei Darussalam.
     *
     * @var string
     */
    const BN = 'BN';

    /**
     * Bulgaria.
     *
     * @var string
     */
    const BG = 'BG';

    /**
     * Burkina Faso.
     *
     * @var string
     */
    const BF = 'BF';

    /**
     * Burundi.
     *
     * @var string
     */
    const BI = 'BI';

    /**
     * Cambodia.
     *
     * @var string
     */
    const KH = 'KH';

    /**
     * Cameroon.
     *
     * @var string
     */
    const CM = 'CM';

    /**
     * Canada.
     *
     * @var string
     */
    const CA = 'CA';

    /**
     * Cape Verde.
     *
     * @var string
     */
    const CV = 'CV';

    /**
     * Cayman Islands.
     *
     * @var string
     */
    const KY = 'KY';

    /**
     * Central African Republic.
     *
     * @var string
     */
    const CF = 'CF';

    /**
     * Chad.
     *
     * @var string
     */
    const TD = 'TD';

    /**
     * Chile.
     *
     * @var string
     */
    const CL = 'CL';

    /**
     * China.
     *
     * @var string
     */
    const CN = 'CN';

    /**
     * Hong Kong, SAR China.
     *
     * @var string
     */
    const HK = 'HK';

    /**
     * Macao, SAR China.
     *
     * @var string
     */
    const MO = 'MO';

    /**
     * Christmas Island.
     *
     * @var string
     */
    const CX = 'CX';

    /**
     * Cocos (Keeling) Islands.
     *
     * @var string
     */
    const CC = 'CC';

    /**
     * Colombia.
     *
     * @var string
     */
    const CO = 'CO';

    /**
     * Comoros.
     *
     * @var string
     */
    const KM = 'KM';

    /**
     * Congo (Brazzaville).
     *
     * @var string
     */
    const CG = 'CG';

    /**
     * Congo, (Kinshasa).
     *
     * @var string
     */
    const CD = 'CD';

    /**
     * Cook Islands.
     *
     * @var string
     */
    const CK = 'CK';

    /**
     * Costa Rica.
     *
     * @var string
     */
    const CR = 'CR';

    /**
     * Côte d\'Ivoire.
     *
     * @var string
     */
    const CI = 'CI';

    /**
     * Croatia.
     *
     * @var string
     */
    const HR = 'HR';

    /**
     * Cuba.
     *
     * @var string
     */
    const CU = 'CU';

    /**
     * Cyprus.
     *
     * @var string
     */
    const CY = 'CY';

    /**
     * Czech Republic.
     *
     * @var string
     */
    const CZ = 'CZ';

    /**
     * Denmark.
     *
     * @var string
     */
    const DK = 'DK';

    /**
     * Djibouti.
     *
     * @var string
     */
    const DJ = 'DJ';

    /**
     * Dominica.
     *
     * @var string
     */
    const DM = 'DM';

    /**
     * Dominican Republic.
     *
     * @var string
     */
    const DO = 'DO';

    /**
     * Ecuador.
     *
     * @var string
     */
    const EC = 'EC';

    /**
     * Egypt.
     *
     * @var string
     */
    const EG = 'EG';

    /**
     * El Salvador.
     *
     * @var string
     */
    const SV = 'SV';

    /**
     * Equatorial Guinea.
     *
     * @var string
     */
    const GQ = 'GQ';

    /**
     * Eritrea.
     *
     * @var string
     */
    const ER = 'ER';

    /**
     * Estonia.
     *
     * @var string
     */
    const EE = 'EE';

    /**
     * Ethiopia.
     *
     * @var string
     */
    const ET = 'ET';

    /**
     * Falkland Islands (Malvinas).
     *
     * @var string
     */
    const FK = 'FK';

    /**
     * Faroe Islands.
     *
     * @var string
     */
    const FO = 'FO';

    /**
     * Fiji.
     *
     * @var string
     */
    const FJ = 'FJ';

    /**
     * Finland.
     *
     * @var string
     */
    const FI = 'FI';

    /**
     * France.
     *
     * @var string
     */
    const FR = 'FR';

    /**
     * French Guiana.
     *
     * @var string
     */
    const GF = 'GF';

    /**
     * French Polynesia.
     *
     * @var string
     */
    const PF = 'PF';

    /**
     * French Southern Territories.
     *
     * @var string
     */
    const TF = 'TF';

    /**
     * Gabon.
     *
     * @var string
     */
    const GA = 'GA';

    /**
     * Gambia.
     *
     * @var string
     */
    const GM = 'GM';

    /**
     * Georgia.
     *
     * @var string
     */
    const GE = 'GE';

    /**
     * Germany.
     *
     * @var string
     */
    const DE = 'DE';

    /**
     * Ghana.
     *
     * @var string
     */
    const GH = 'GH';

    /**
     * Gibraltar.
     *
     * @var string
     */
    const GI = 'GI';

    /**
     * Greece.
     *
     * @var string
     */
    const GR = 'GR';

    /**
     * Greenland.
     *
     * @var string
     */
    const GL = 'GL';

    /**
     * Grenada.
     *
     * @var string
     */
    const GD = 'GD';

    /**
     * Guadeloupe.
     *
     * @var string
     */
    const GP = 'GP';

    /**
     * Guam.
     *
     * @var string
     */
    const GU = 'GU';

    /**
     * Guatemala.
     *
     * @var string
     */
    const GT = 'GT';

    /**
     * Guernsey.
     *
     * @var string
     */
    const GG = 'GG';

    /**
     * Guinea.
     *
     * @var string
     */
    const GN = 'GN';

    /**
     * Guinea-Bissau.
     *
     * @var string
     */
    const GW = 'GW';

    /**
     * Guyana.
     *
     * @var string
     */
    const GY = 'GY';

    /**
     * Haiti.
     *
     * @var string
     */
    const HT = 'HT';

    /**
     * Heard and Mcdonald Islands.
     *
     * @var string
     */
    const HM = 'HM';

    /**
     * Holy See (Vatican City State).
     *
     * @var string
     */
    const VA = 'VA';

    /**
     * Honduras.
     *
     * @var string
     */
    const HN = 'HN';

    /**
     * Hungary.
     *
     * @var string
     */
    const HU = 'HU';

    /**
     * Iceland.
     *
     * @var string
     */
    const IS = 'IS';

    /**
     * India.
     *
     * @var string
     */
    const IN = 'IN';

    /**
     * Indonesia.
     *
     * @var string
     */
    const ID = 'ID';

    /**
     * Iran, Islamic Republic of.
     *
     * @var string
     */
    const IR = 'IR';

    /**
     * Iraq.
     *
     * @var string
     */
    const IQ = 'IQ';

    /**
     * Ireland.
     *
     * @var string
     */
    const IE = 'IE';

    /**
     * Isle of Man.
     *
     * @var string
     */
    const IM = 'IM';

    /**
     * Israel.
     *
     * @var string
     */
    const IL = 'IL';

    /**
     * Italy.
     *
     * @var string
     */
    const IT = 'IT';

    /**
     * Jamaica.
     *
     * @var string
     */
    const JM = 'JM';

    /**
     * Japan.
     *
     * @var string
     */
    const JP = 'JP';

    /**
     * Jersey.
     *
     * @var string
     */
    const JE = 'JE';

    /**
     * Jordan.
     *
     * @var string
     */
    const JO = 'JO';

    /**
     * Kazakhstan.
     *
     * @var string
     */
    const KZ = 'KZ';

    /**
     * Kenya.
     *
     * @var string
     */
    const KE = 'KE';

    /**
     * Kiribati.
     *
     * @var string
     */
    const KI = 'KI';

    /**
     * Korea (North).
     *
     * @var string
     */
    const KP = 'KP';

    /**
     * Korea (South).
     *
     * @var string
     */
    const KR = 'KR';

    /**
     * Kuwait.
     *
     * @var string
     */
    const KW = 'KW';

    /**
     * Kyrgyzstan.
     *
     * @var string
     */
    const KG = 'KG';

    /**
     * Lao PDR.
     *
     * @var string
     */
    const LA = 'LA';

    /**
     * Latvia.
     *
     * @var string
     */
    const LV = 'LV';

    /**
     * Lebanon.
     *
     * @var string
     */
    const LB = 'LB';

    /**
     * Lesotho.
     *
     * @var string
     */
    const LS = 'LS';

    /**
     * Liberia.
     *
     * @var string
     */
    const LR = 'LR';

    /**
     * Libya.
     *
     * @var string
     */
    const LY = 'LY';

    /**
     * Liechtenstein.
     *
     * @var string
     */
    const LI = 'LI';

    /**
     * Lithuania.
     *
     * @var string
     */
    const LT = 'LT';

    /**
     * Luxembourg.
     *
     * @var string
     */
    const LU = 'LU';

    /**
     * Macedonia, Republic of.
     *
     * @var string
     */
    const MK = 'MK';

    /**
     * Madagascar.
     *
     * @var string
     */
    const MG = 'MG';

    /**
     * Malawi.
     *
     * @var string
     */
    const MW = 'MW';

    /**
     * Malaysia.
     *
     * @var string
     */
    const MY = 'MY';

    /**
     * Maldives.
     *
     * @var string
     */
    const MV = 'MV';

    /**
     * Mali.
     *
     * @var string
     */
    const ML = 'ML';

    /**
     * Malta.
     *
     * @var string
     */
    const MT = 'MT';

    /**
     * Marshall Islands.
     *
     * @var string
     */
    const MH = 'MH';

    /**
     * Martinique.
     *
     * @var string
     */
    const MQ = 'MQ';

    /**
     * Mauritania.
     *
     * @var string
     */
    const MR = 'MR';

    /**
     * Mauritius.
     *
     * @var string
     */
    const MU = 'MU';

    /**
     * Mayotte.
     *
     * @var string
     */
    const YT = 'YT';

    /**
     * Mexico.
     *
     * @var string
     */
    const MX = 'MX';

    /**
     * Micronesia, Federated States of.
     *
     * @var string
     */
    const FM = 'FM';

    /**
     * Moldova.
     *
     * @var string
     */
    const MD = 'MD';

    /**
     * Monaco.
     *
     * @var string
     */
    const MC = 'MC';

    /**
     * Mongolia.
     *
     * @var string
     */
    const MN = 'MN';

    /**
     * Montenegro.
     *
     * @var string
     */
    const ME = 'ME';

    /**
     * Montserrat.
     *
     * @var string
     */
    const MS = 'MS';

    /**
     * Morocco.
     *
     * @var string
     */
    const MA = 'MA';

    /**
     * Mozambique.
     *
     * @var string
     */
    const MZ = 'MZ';

    /**
     * Myanmar.
     *
     * @var string
     */
    const MM = 'MM';

    /**
     * Namibia.
     *
     * @var string
     */
    const NA = 'NA';

    /**
     * Nauru.
     *
     * @var string
     */
    const NR = 'NR';

    /**
     * Nepal.
     *
     * @var string
     */
    const NP = 'NP';

    /**
     * Netherlands.
     *
     * @var string
     */
    const NL = 'NL';

    /**
     * Netherlands Antilles.
     *
     * @var string
     */
    const AN = 'AN';

    /**
     * New Caledonia.
     *
     * @var string
     */
    const NC = 'NC';

    /**
     * New Zealand.
     *
     * @var string
     */
    const NZ = 'NZ';

    /**
     * Nicaragua.
     *
     * @var string
     */
    const NI = 'NI';

    /**
     * Niger.
     *
     * @var string
     */
    const NE = 'NE';

    /**
     * Nigeria.
     *
     * @var string
     */
    const NG = 'NG';

    /**
     * Niue.
     *
     * @var string
     */
    const NU = 'NU';

    /**
     * Norfolk Island.
     *
     * @var string
     */
    const NF = 'NF';

    /**
     * Northern Mariana Islands.
     *
     * @var string
     */
    const MP = 'MP';

    /**
     * Norway.
     *
     * @var string
     */
    const NO = 'NO';

    /**
     * Oman.
     *
     * @var string
     */
    const OM = 'OM';

    /**
     * Pakistan.
     *
     * @var string
     */
    const PK = 'PK';

    /**
     * Palau.
     *
     * @var string
     */
    const PW = 'PW';

    /**
     * Palestinian Territory.
     *
     * @var string
     */
    const PS = 'PS';

    /**
     * Panama.
     *
     * @var string
     */
    const PA = 'PA';

    /**
     * Papua New Guinea.
     *
     * @var string
     */
    const PG = 'PG';

    /**
     * Paraguay.
     *
     * @var string
     */
    const PY = 'PY';

    /**
     * Peru.
     *
     * @var string
     */
    const PE = 'PE';

    /**
     * Philippines.
     *
     * @var string
     */
    const PH = 'PH';

    /**
     * Pitcairn.
     *
     * @var string
     */
    const PN = 'PN';

    /**
     * Poland.
     *
     * @var string
     */
    const PL = 'PL';

    /**
     * Portugal.
     *
     * @var string
     */
    const PT = 'PT';

    /**
     * Puerto Rico.
     *
     * @var string
     */
    const PR = 'PR';

    /**
     * Qatar.
     *
     * @var string
     */
    const QA = 'QA';

    /**
     * Réunion.
     *
     * @var string
     */
    const RE = 'RE';

    /**
     * Romania.
     *
     * @var string
     */
    const RO = 'RO';

    /**
     * Russian Federation.
     *
     * @var string
     */
    const RU = 'RU';

    /**
     * Rwanda.
     *
     * @var string
     */
    const RW = 'RW';

    /**
     * Saint-Barthélemy.
     *
     * @var string
     */
    const BL = 'BL';

    /**
     * Saint Helena.
     *
     * @var string
     */
    const SH = 'SH';

    /**
     * Saint Kitts and Nevis.
     *
     * @var string
     */
    const KN = 'KN';

    /**
     * Saint Lucia.
     *
     * @var string
     */
    const LC = 'LC';

    /**
     * Saint-Martin (French part).
     *
     * @var string
     */
    const MF = 'MF';

    /**
     * Saint Pierre and Miquelon.
     *
     * @var string
     */
    const PM = 'PM';

    /**
     * Saint Vincent and Grenadines.
     *
     * @var string
     */
    const VC = 'VC';

    /**
     * Samoa.
     *
     * @var string
     */
    const WS = 'WS';

    /**
     * San Marino.
     *
     * @var string
     */
    const SM = 'SM';

    /**
     * Sao Tome and Principe.
     *
     * @var string
     */
    const ST = 'ST';

    /**
     * Saudi Arabia.
     *
     * @var string
     */
    const SA = 'SA';

    /**
     * Senegal.
     *
     * @var string
     */
    const SN = 'SN';

    /**
     * Serbia.
     *
     * @var string
     */
    const RS = 'RS';

    /**
     * Seychelles.
     *
     * @var string
     */
    const SC = 'SC';

    /**
     * Sierra Leone.
     *
     * @var string
     */
    const SL = 'SL';

    /**
     * Singapore.
     *
     * @var string
     */
    const SG = 'SG';

    /**
     * Slovakia.
     *
     * @var string
     */
    const SK = 'SK';

    /**
     * Slovenia.
     *
     * @var string
     */
    const SI = 'SI';

    /**
     * Solomon Islands.
     *
     * @var string
     */
    const SB = 'SB';

    /**
     * Somalia.
     *
     * @var string
     */
    const SO = 'SO';

    /**
     * South Africa.
     *
     * @var string
     */
    const ZA = 'ZA';

    /**
     * South Georgia and the South Sandwich Islands.
     *
     * @var string
     */
    const GS = 'GS';

    /**
     * South Sudan.
     *
     * @var string
     */
    const SS = 'SS';

    /**
     * Spain.
     *
     * @var string
     */
    const ES = 'ES';

    /**
     * Sri Lanka.
     *
     * @var string
     */
    const LK = 'LK';

    /**
     * Sudan.
     *
     * @var string
     */
    const SD = 'SD';

    /**
     * Suriname.
     *
     * @var string
     */
    const SR = 'SR';

    /**
     * Svalbard and Jan Mayen Islands.
     *
     * @var string
     */
    const SJ = 'SJ';

    /**
     * Swaziland.
     *
     * @var string
     */
    const SZ = 'SZ';

    /**
     * Sweden.
     *
     * @var string
     */
    const SE = 'SE';

    /**
     * Switzerland.
     *
     * @var string
     */
    const CH = 'CH';

    /**
     * Syrian Arab Republic (Syria).
     *
     * @var string
     */
    const SY = 'SY';

    /**
     * Taiwan, Republic of China.
     *
     * @var string
     */
    const TW = 'TW';

    /**
     * Tajikistan.
     *
     * @var string
     */
    const TJ = 'TJ';

    /**
     * Tanzania, United Republic of.
     *
     * @var string
     */
    const TZ = 'TZ';

    /**
     * Thailand.
     *
     * @var string
     */
    const TH = 'TH';

    /**
     * Timor-Leste.
     *
     * @var string
     */
    const TL = 'TL';

    /**
     * Togo.
     *
     * @var string
     */
    const TG = 'TG';

    /**
     * Tokelau.
     *
     * @var string
     */
    const TK = 'TK';

    /**
     * Tonga.
     *
     * @var string
     */
    const TO = 'TO';

    /**
     * Trinidad and Tobago.
     *
     * @var string
     */
    const TT = 'TT';

    /**
     * Tunisia.
     *
     * @var string
     */
    const TN = 'TN';

    /**
     * Turkey.
     *
     * @var string
     */
    const TR = 'TR';

    /**
     * Turkmenistan.
     *
     * @var string
     */
    const TM = 'TM';

    /**
     * Turks and Caicos Islands.
     *
     * @var string
     */
    const TC = 'TC';

    /**
     * Tuvalu.
     *
     * @var string
     */
    const TV = 'TV';

    /**
     * Uganda.
     *
     * @var string
     */
    const UG = 'UG';

    /**
     * Ukraine.
     *
     * @var string
     */
    const UA = 'UA';

    /**
     * United Arab Emirates.
     *
     * @var string
     */
    const AE = 'AE';

    /**
     * United Kingdom.
     *
     * @var string
     */
    const GB = 'GB';

    /**
     * United States of America.
     *
     * @var string
     */
    const US = 'US';

    /**
     * US Minor Outlying Islands.
     *
     * @var string
     */
    const UM = 'UM';

    /**
     * Uruguay.
     *
     * @var string
     */
    const UY = 'UY';

    /**
     * Uzbekistan.
     *
     * @var string
     */
    const UZ = 'UZ';

    /**
     * Vanuatu.
     *
     * @var string
     */
    const VU = 'VU';

    /**
     * Venezuela (Bolivarian Republic).
     *
     * @var string
     */
    const VE = 'VE';

    /**
     * Viet Nam.
     *
     * @var string
     */
    const VN = 'VN';

    /**
     * Virgin Islands, US.
     *
     * @var string
     */
    const VI = 'VI';

    /**
     * Wallis and Futuna Islands.
     *
     * @var string
     */
    const WF = 'WF';

    /**
     * Western Sahara.
     *
     * @var string
     */
    const EH = 'EH';

    /**
     * Yemen.
     *
     * @var string
     */
    const YE = 'YE';

    /**
     * Zambia.
     *
     * @var string
     */
    const ZM = 'ZM';

    /**
     * Zimbabwe.
     *
     * @var string
     */
    const ZW = 'ZW';

    /**
     * Returns a list of localized labels for each enum.
     *
     * @param string $lang Language code
     *
     * @return array
     */
    public static function labels(string $lang = 'en')
    {
        $en = [
            self::AF => 'Afghanistan',
            self::AX => 'Aland Islands',
            self::AL => 'Albania',
            self::DZ => 'Algeria',
            self::AS => 'American Samoa',
            self::AD => 'Andorra',
            self::AO => 'Angola',
            self::AI => 'Anguilla',
            self::AQ => 'Antarctica',
            self::AG => 'Antigua and Barbuda',
            self::AR => 'Argentina',
            self::AM => 'Armenia',
            self::AW => 'Aruba',
            self::AU => 'Australia',
            self::AT => 'Austria',
            self::AZ => 'Azerbaijan',
            self::BS => 'Bahamas',
            self::BH => 'Bahrain',
            self::BD => 'Bangladesh',
            self::BB => 'Barbados',
            self::BY => 'Belarus',
            self::BE => 'Belgium',
            self::BZ => 'Belize',
            self::BJ => 'Benin',
            self::BM => 'Bermuda',
            self::BT => 'Bhutan',
            self::BO => 'Bolivia',
            self::BA => 'Bosnia and Herzegovina',
            self::BW => 'Botswana',
            self::BV => 'Bouvet Island',
            self::BR => 'Brazil',
            self::VG => 'British Virgin Islands',
            self::IO => 'British Indian Ocean Territory',
            self::BN => 'Brunei Darussalam',
            self::BG => 'Bulgaria',
            self::BF => 'Burkina Faso',
            self::BI => 'Burundi',
            self::KH => 'Cambodia',
            self::CM => 'Cameroon',
            self::CA => 'Canada',
            self::CV => 'Cape Verde',
            self::KY => 'Cayman Islands',
            self::CF => 'Central African Republic',
            self::TD => 'Chad',
            self::CL => 'Chile',
            self::CN => 'China',
            self::HK => 'Hong Kong, SAR China',
            self::MO => 'Macao, SAR China',
            self::CX => 'Christmas Island',
            self::CC => 'Cocos (Keeling) Islands',
            self::CO => 'Colombia',
            self::KM => 'Comoros',
            self::CG => 'Congo (Brazzaville)',
            self::CD => 'Congo, (Kinshasa)',
            self::CK => 'Cook Islands',
            self::CR => 'Costa Rica',
            self::CI => 'Côte d\'Ivoire',
            self::HR => 'Croatia',
            self::CU => 'Cuba',
            self::CY => 'Cyprus',
            self::CZ => 'Czech Republic',
            self::DK => 'Denmark',
            self::DJ => 'Djibouti',
            self::DM => 'Dominica',
            self::DO => 'Dominican Republic',
            self::EC => 'Ecuador',
            self::EG => 'Egypt',
            self::SV => 'El Salvador',
            self::GQ => 'Equatorial Guinea',
            self::ER => 'Eritrea',
            self::EE => 'Estonia',
            self::ET => 'Ethiopia',
            self::FK => 'Falkland Islands (Malvinas)',
            self::FO => 'Faroe Islands',
            self::FJ => 'Fiji',
            self::FI => 'Finland',
            self::FR => 'France',
            self::GF => 'French Guiana',
            self::PF => 'French Polynesia',
            self::TF => 'French Southern Territories',
            self::GA => 'Gabon',
            self::GM => 'Gambia',
            self::GE => 'Georgia',
            self::DE => 'Germany',
            self::GH => 'Ghana',
            self::GI => 'Gibraltar',
            self::GR => 'Greece',
            self::GL => 'Greenland',
            self::GD => 'Grenada',
            self::GP => 'Guadeloupe',
            self::GU => 'Guam',
            self::GT => 'Guatemala',
            self::GG => 'Guernsey',
            self::GN => 'Guinea',
            self::GW => 'Guinea-Bissau',
            self::GY => 'Guyana',
            self::HT => 'Haiti',
            self::HM => 'Heard and Mcdonald Islands',
            self::VA => 'Holy See (Vatican City State)',
            self::HN => 'Honduras',
            self::HU => 'Hungary',
            self::IS => 'Iceland',
            self::IN => 'India',
            self::ID => 'Indonesia',
            self::IR => 'Iran, Islamic Republic of',
            self::IQ => 'Iraq',
            self::IE => 'Ireland',
            self::IM => 'Isle of Man',
            self::IL => 'Israel',
            self::IT => 'Italy',
            self::JM => 'Jamaica',
            self::JP => 'Japan',
            self::JE => 'Jersey',
            self::JO => 'Jordan',
            self::KZ => 'Kazakhstan',
            self::KE => 'Kenya',
            self::KI => 'Kiribati',
            self::KP => 'Korea (North)',
            self::KR => 'Korea (South)',
            self::KW => 'Kuwait',
            self::KG => 'Kyrgyzstan',
            self::LA => 'Lao PDR',
            self::LV => 'Latvia',
            self::LB => 'Lebanon',
            self::LS => 'Lesotho',
            self::LR => 'Liberia',
            self::LY => 'Libya',
            self::LI => 'Liechtenstein',
            self::LT => 'Lithuania',
            self::LU => 'Luxembourg',
            self::MK => 'Macedonia, Republic of',
            self::MG => 'Madagascar',
            self::MW => 'Malawi',
            self::MY => 'Malaysia',
            self::MV => 'Maldives',
            self::ML => 'Mali',
            self::MT => 'Malta',
            self::MH => 'Marshall Islands',
            self::MQ => 'Martinique',
            self::MR => 'Mauritania',
            self::MU => 'Mauritius',
            self::YT => 'Mayotte',
            self::MX => 'Mexico',
            self::FM => 'Micronesia, Federated States of',
            self::MD => 'Moldova',
            self::MC => 'Monaco',
            self::MN => 'Mongolia',
            self::ME => 'Montenegro',
            self::MS => 'Montserrat',
            self::MA => 'Morocco',
            self::MZ => 'Mozambique',
            self::MM => 'Myanmar',
            self::NA => 'Namibia',
            self::NR => 'Nauru',
            self::NP => 'Nepal',
            self::NL => 'Netherlands',
            self::AN => 'Netherlands Antilles',
            self::NC => 'New Caledonia',
            self::NZ => 'New Zealand',
            self::NI => 'Nicaragua',
            self::NE => 'Niger',
            self::NG => 'Nigeria',
            self::NU => 'Niue',
            self::NF => 'Norfolk Island',
            self::MP => 'Northern Mariana Islands',
            self::NO => 'Norway',
            self::OM => 'Oman',
            self::PK => 'Pakistan',
            self::PW => 'Palau',
            self::PS => 'Palestinian Territory',
            self::PA => 'Panama',
            self::PG => 'Papua New Guinea',
            self::PY => 'Paraguay',
            self::PE => 'Peru',
            self::PH => 'Philippines',
            self::PN => 'Pitcairn',
            self::PL => 'Poland',
            self::PT => 'Portugal',
            self::PR => 'Puerto Rico',
            self::QA => 'Qatar',
            self::RE => 'Réunion',
            self::RO => 'Romania',
            self::RU => 'Russian Federation',
            self::RW => 'Rwanda',
            self::BL => 'Saint-Barthélemy',
            self::SH => 'Saint Helena',
            self::KN => 'Saint Kitts and Nevis',
            self::LC => 'Saint Lucia',
            self::MF => 'Saint-Martin (French part)',
            self::PM => 'Saint Pierre and Miquelon',
            self::VC => 'Saint Vincent and Grenadines',
            self::WS => 'Samoa',
            self::SM => 'San Marino',
            self::ST => 'Sao Tome and Principe',
            self::SA => 'Saudi Arabia',
            self::SN => 'Senegal',
            self::RS => 'Serbia',
            self::SC => 'Seychelles',
            self::SL => 'Sierra Leone',
            self::SG => 'Singapore',
            self::SK => 'Slovakia',
            self::SI => 'Slovenia',
            self::SB => 'Solomon Islands',
            self::SO => 'Somalia',
            self::ZA => 'South Africa',
            self::GS => 'South Georgia and the South Sandwich Islands',
            self::SS => 'South Sudan',
            self::ES => 'Spain',
            self::LK => 'Sri Lanka',
            self::SD => 'Sudan',
            self::SR => 'Suriname',
            self::SJ => 'Svalbard and Jan Mayen Islands',
            self::SZ => 'Swaziland',
            self::SE => 'Sweden',
            self::CH => 'Switzerland',
            self::SY => 'Syrian Arab Republic (Syria)',
            self::TW => 'Taiwan, Republic of China',
            self::TJ => 'Tajikistan',
            self::TZ => 'Tanzania, United Republic of',
            self::TH => 'Thailand',
            self::TL => 'Timor-Leste',
            self::TG => 'Togo',
            self::TK => 'Tokelau',
            self::TO => 'Tonga',
            self::TT => 'Trinidad and Tobago',
            self::TN => 'Tunisia',
            self::TR => 'Turkey',
            self::TM => 'Turkmenistan',
            self::TC => 'Turks and Caicos Islands',
            self::TV => 'Tuvalu',
            self::UG => 'Uganda',
            self::UA => 'Ukraine',
            self::AE => 'United Arab Emirates',
            self::GB => 'United Kingdom',
            self::US => 'United States of America',
            self::UM => 'US Minor Outlying Islands',
            self::UY => 'Uruguay',
            self::UZ => 'Uzbekistan',
            self::VU => 'Vanuatu',
            self::VE => 'Venezuela (Bolivarian Republic)',
            self::VN => 'Viet Nam',
            self::VI => 'Virgin Islands, US',
            self::WF => 'Wallis and Futuna Islands',
            self::EH => 'Western Sahara',
            self::YE => 'Yemen',
            self::ZM => 'Zambia',
            self::ZW => 'Zimbabwe',
        ];

        $fr = [
            self::AF => 'Afghanistan',
            self::AX => 'Aland Islands',
            self::AL => 'Albania',
            self::DZ => 'Algeria',
            self::AS => 'American Samoa',
            self::AD => 'Andorra',
            self::AO => 'Angola',
            self::AI => 'Anguilla',
            self::AQ => 'Antarctica',
            self::AG => 'Antigua and Barbuda',
            self::AR => 'Argentina',
            self::AM => 'Armenia',
            self::AW => 'Aruba',
            self::AU => 'Australia',
            self::AT => 'Austria',
            self::AZ => 'Azerbaijan',
            self::BS => 'Bahamas',
            self::BH => 'Bahrain',
            self::BD => 'Bangladesh',
            self::BB => 'Barbados',
            self::BY => 'Belarus',
            self::BE => 'Belgium',
            self::BZ => 'Belize',
            self::BJ => 'Benin',
            self::BM => 'Bermuda',
            self::BT => 'Bhutan',
            self::BO => 'Bolivia',
            self::BA => 'Bosnia and Herzegovina',
            self::BW => 'Botswana',
            self::BV => 'Bouvet Island',
            self::BR => 'Brazil',
            self::VG => 'British Virgin Islands',
            self::IO => 'British Indian Ocean Territory',
            self::BN => 'Brunei Darussalam',
            self::BG => 'Bulgaria',
            self::BF => 'Burkina Faso',
            self::BI => 'Burundi',
            self::KH => 'Cambodia',
            self::CM => 'Cameroon',
            self::CA => 'Canada',
            self::CV => 'Cape Verde',
            self::KY => 'Cayman Islands',
            self::CF => 'Central African Republic',
            self::TD => 'Chad',
            self::CL => 'Chile',
            self::CN => 'China',
            self::HK => 'Hong Kong, SAR China',
            self::MO => 'Macao, SAR China',
            self::CX => 'Christmas Island',
            self::CC => 'Cocos (Keeling) Islands',
            self::CO => 'Colombia',
            self::KM => 'Comoros',
            self::CG => 'Congo (Brazzaville)',
            self::CD => 'Congo, (Kinshasa)',
            self::CK => 'Cook Islands',
            self::CR => 'Costa Rica',
            self::CI => 'Côte d\'Ivoire',
            self::HR => 'Croatia',
            self::CU => 'Cuba',
            self::CY => 'Cyprus',
            self::CZ => 'Czech Republic',
            self::DK => 'Denmark',
            self::DJ => 'Djibouti',
            self::DM => 'Dominica',
            self::DO => 'Dominican Republic',
            self::EC => 'Ecuador',
            self::EG => 'Egypt',
            self::SV => 'El Salvador',
            self::GQ => 'Equatorial Guinea',
            self::ER => 'Eritrea',
            self::EE => 'Estonia',
            self::ET => 'Ethiopia',
            self::FK => 'Falkland Islands (Malvinas)',
            self::FO => 'Faroe Islands',
            self::FJ => 'Fiji',
            self::FI => 'Finland',
            self::FR => 'France',
            self::GF => 'French Guiana',
            self::PF => 'French Polynesia',
            self::TF => 'French Southern Territories',
            self::GA => 'Gabon',
            self::GM => 'Gambia',
            self::GE => 'Georgia',
            self::DE => 'Germany',
            self::GH => 'Ghana',
            self::GI => 'Gibraltar',
            self::GR => 'Greece',
            self::GL => 'Greenland',
            self::GD => 'Grenada',
            self::GP => 'Guadeloupe',
            self::GU => 'Guam',
            self::GT => 'Guatemala',
            self::GG => 'Guernsey',
            self::GN => 'Guinea',
            self::GW => 'Guinea-Bissau',
            self::GY => 'Guyana',
            self::HT => 'Haiti',
            self::HM => 'Heard and Mcdonald Islands',
            self::VA => 'Holy See (Vatican City State)',
            self::HN => 'Honduras',
            self::HU => 'Hungary',
            self::IS => 'Iceland',
            self::IN => 'India',
            self::ID => 'Indonesia',
            self::IR => 'Iran, Islamic Republic of',
            self::IQ => 'Iraq',
            self::IE => 'Ireland',
            self::IM => 'Isle of Man',
            self::IL => 'Israel',
            self::IT => 'Italy',
            self::JM => 'Jamaica',
            self::JP => 'Japan',
            self::JE => 'Jersey',
            self::JO => 'Jordan',
            self::KZ => 'Kazakhstan',
            self::KE => 'Kenya',
            self::KI => 'Kiribati',
            self::KP => 'Korea (North)',
            self::KR => 'Korea (South)',
            self::KW => 'Kuwait',
            self::KG => 'Kyrgyzstan',
            self::LA => 'Lao PDR',
            self::LV => 'Latvia',
            self::LB => 'Lebanon',
            self::LS => 'Lesotho',
            self::LR => 'Liberia',
            self::LY => 'Libya',
            self::LI => 'Liechtenstein',
            self::LT => 'Lithuania',
            self::LU => 'Luxembourg',
            self::MK => 'Macedonia, Republic of',
            self::MG => 'Madagascar',
            self::MW => 'Malawi',
            self::MY => 'Malaysia',
            self::MV => 'Maldives',
            self::ML => 'Mali',
            self::MT => 'Malta',
            self::MH => 'Marshall Islands',
            self::MQ => 'Martinique',
            self::MR => 'Mauritania',
            self::MU => 'Mauritius',
            self::YT => 'Mayotte',
            self::MX => 'Mexico',
            self::FM => 'Micronesia, Federated States of',
            self::MD => 'Moldova',
            self::MC => 'Monaco',
            self::MN => 'Mongolia',
            self::ME => 'Montenegro',
            self::MS => 'Montserrat',
            self::MA => 'Morocco',
            self::MZ => 'Mozambique',
            self::MM => 'Myanmar',
            self::NA => 'Namibia',
            self::NR => 'Nauru',
            self::NP => 'Nepal',
            self::NL => 'Netherlands',
            self::AN => 'Netherlands Antilles',
            self::NC => 'New Caledonia',
            self::NZ => 'New Zealand',
            self::NI => 'Nicaragua',
            self::NE => 'Niger',
            self::NG => 'Nigeria',
            self::NU => 'Niue',
            self::NF => 'Norfolk Island',
            self::MP => 'Northern Mariana Islands',
            self::NO => 'Norway',
            self::OM => 'Oman',
            self::PK => 'Pakistan',
            self::PW => 'Palau',
            self::PS => 'Palestinian Territory',
            self::PA => 'Panama',
            self::PG => 'Papua New Guinea',
            self::PY => 'Paraguay',
            self::PE => 'Peru',
            self::PH => 'Philippines',
            self::PN => 'Pitcairn',
            self::PL => 'Poland',
            self::PT => 'Portugal',
            self::PR => 'Puerto Rico',
            self::QA => 'Qatar',
            self::RE => 'Réunion',
            self::RO => 'Romania',
            self::RU => 'Russian Federation',
            self::RW => 'Rwanda',
            self::BL => 'Saint-Barthélemy',
            self::SH => 'Saint Helena',
            self::KN => 'Saint Kitts and Nevis',
            self::LC => 'Saint Lucia',
            self::MF => 'Saint-Martin (French part)',
            self::PM => 'Saint Pierre and Miquelon',
            self::VC => 'Saint Vincent and Grenadines',
            self::WS => 'Samoa',
            self::SM => 'San Marino',
            self::ST => 'Sao Tome and Principe',
            self::SA => 'Saudi Arabia',
            self::SN => 'Senegal',
            self::RS => 'Serbia',
            self::SC => 'Seychelles',
            self::SL => 'Sierra Leone',
            self::SG => 'Singapore',
            self::SK => 'Slovakia',
            self::SI => 'Slovenia',
            self::SB => 'Solomon Islands',
            self::SO => 'Somalia',
            self::ZA => 'South Africa',
            self::GS => 'South Georgia and the South Sandwich Islands',
            self::SS => 'South Sudan',
            self::ES => 'Spain',
            self::LK => 'Sri Lanka',
            self::SD => 'Sudan',
            self::SR => 'Suriname',
            self::SJ => 'Svalbard and Jan Mayen Islands',
            self::SZ => 'Swaziland',
            self::SE => 'Sweden',
            self::CH => 'Switzerland',
            self::SY => 'Syrian Arab Republic (Syria)',
            self::TW => 'Taiwan, Republic of China',
            self::TJ => 'Tajikistan',
            self::TZ => 'Tanzania, United Republic of',
            self::TH => 'Thailand',
            self::TL => 'Timor-Leste',
            self::TG => 'Togo',
            self::TK => 'Tokelau',
            self::TO => 'Tonga',
            self::TT => 'Trinidad and Tobago',
            self::TN => 'Tunisia',
            self::TR => 'Turkey',
            self::TM => 'Turkmenistan',
            self::TC => 'Turks and Caicos Islands',
            self::TV => 'Tuvalu',
            self::UG => 'Uganda',
            self::UA => 'Ukraine',
            self::AE => 'United Arab Emirates',
            self::GB => 'United Kingdom',
            self::US => 'United States of America',
            self::UM => 'US Minor Outlying Islands',
            self::UY => 'Uruguay',
            self::UZ => 'Uzbekistan',
            self::VU => 'Vanuatu',
            self::VE => 'Venezuela (Bolivarian Republic)',
            self::VN => 'Viet Nam',
            self::VI => 'Virgin Islands, US',
            self::WF => 'Wallis and Futuna Islands',
            self::EH => 'Western Sahara',
            self::YE => 'Yemen',
            self::ZM => 'Zambia',
            self::ZW => 'Zimbabwe',
        ];

        return $$lang;
    }

    /**
     * Returns the list of enums in this class.
     *
     * @return string[]
     */
    public static function enums()
    {
        return [
            self::AF,
            self::AX,
            self::AL,
            self::DZ,
            self::AS,
            self::AD,
            self::AO,
            self::AI,
            self::AQ,
            self::AG,
            self::AR,
            self::AM,
            self::AW,
            self::AU,
            self::AT,
            self::AZ,
            self::BS,
            self::BH,
            self::BD,
            self::BB,
            self::BY,
            self::BE,
            self::BZ,
            self::BJ,
            self::BM,
            self::BT,
            self::BO,
            self::BA,
            self::BW,
            self::BV,
            self::BR,
            self::VG,
            self::IO,
            self::BN,
            self::BG,
            self::BF,
            self::BI,
            self::KH,
            self::CM,
            self::CA,
            self::CV,
            self::KY,
            self::CF,
            self::TD,
            self::CL,
            self::CN,
            self::HK,
            self::MO,
            self::CX,
            self::CC,
            self::CO,
            self::KM,
            self::CG,
            self::CD,
            self::CK,
            self::CR,
            self::CI,
            self::HR,
            self::CU,
            self::CY,
            self::CZ,
            self::DK,
            self::DJ,
            self::DM,
            self::DO,
            self::EC,
            self::EG,
            self::SV,
            self::GQ,
            self::ER,
            self::EE,
            self::ET,
            self::FK,
            self::FO,
            self::FJ,
            self::FI,
            self::FR,
            self::GF,
            self::PF,
            self::TF,
            self::GA,
            self::GM,
            self::GE,
            self::DE,
            self::GH,
            self::GI,
            self::GR,
            self::GL,
            self::GD,
            self::GP,
            self::GU,
            self::GT,
            self::GG,
            self::GN,
            self::GW,
            self::GY,
            self::HT,
            self::HM,
            self::VA,
            self::HN,
            self::HU,
            self::IS,
            self::IN,
            self::ID,
            self::IR,
            self::IQ,
            self::IE,
            self::IM,
            self::IL,
            self::IT,
            self::JM,
            self::JP,
            self::JE,
            self::JO,
            self::KZ,
            self::KE,
            self::KI,
            self::KP,
            self::KR,
            self::KW,
            self::KG,
            self::LA,
            self::LV,
            self::LB,
            self::LS,
            self::LR,
            self::LY,
            self::LI,
            self::LT,
            self::LU,
            self::MK,
            self::MG,
            self::MW,
            self::MY,
            self::MV,
            self::ML,
            self::MT,
            self::MH,
            self::MQ,
            self::MR,
            self::MU,
            self::YT,
            self::MX,
            self::FM,
            self::MD,
            self::MC,
            self::MN,
            self::ME,
            self::MS,
            self::MA,
            self::MZ,
            self::MM,
            self::NA,
            self::NR,
            self::NP,
            self::NL,
            self::AN,
            self::NC,
            self::NZ,
            self::NI,
            self::NE,
            self::NG,
            self::NU,
            self::NF,
            self::MP,
            self::NO,
            self::OM,
            self::PK,
            self::PW,
            self::PS,
            self::PA,
            self::PG,
            self::PY,
            self::PE,
            self::PH,
            self::PN,
            self::PL,
            self::PT,
            self::PR,
            self::QA,
            self::RE,
            self::RO,
            self::RU,
            self::RW,
            self::BL,
            self::SH,
            self::KN,
            self::LC,
            self::MF,
            self::PM,
            self::VC,
            self::WS,
            self::SM,
            self::ST,
            self::SA,
            self::SN,
            self::RS,
            self::SC,
            self::SL,
            self::SG,
            self::SK,
            self::SI,
            self::SB,
            self::SO,
            self::ZA,
            self::GS,
            self::SS,
            self::ES,
            self::LK,
            self::SD,
            self::SR,
            self::SJ,
            self::SZ,
            self::SE,
            self::CH,
            self::SY,
            self::TW,
            self::TJ,
            self::TZ,
            self::TH,
            self::TL,
            self::TG,
            self::TK,
            self::TO,
            self::TT,
            self::TN,
            self::TR,
            self::TM,
            self::TC,
            self::TV,
            self::UG,
            self::UA,
            self::AE,
            self::GB,
            self::US,
            self::UM,
            self::UY,
            self::UZ,
            self::VU,
            self::VE,
            self::VN,
            self::VI,
            self::WF,
            self::EH,
            self::YE,
            self::ZM,
            self::ZW,
        ];
    }
}
