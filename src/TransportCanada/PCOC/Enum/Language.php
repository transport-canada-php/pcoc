<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Enum;

/**
 * Namespace for language enums.
 */
class Language implements Enum
{
    /**
     * Bilingual enum.
     *
     * @var string
     */
    const BILINGUAL = 'BILINGUAL';

    /**
     * English enum.
     *
     * @var string
     */
    const ENGLISH = 'ENGLISH';

    /**
     * French enum.
     *
     * @var string
     */
    const FRENCH = 'FRENCH';

    /**
     * Not specified enum.
     *
     * @var string
     */
    const NOTSPECIFIED = 'NOTSPECIFIED';

    /**
     * Returns a list of localized labels for each enum.
     *
     * @param string $lang Language code
     *
     * @return array
     */
    public static function labels(string $lang = 'en')
    {
        $en = [
            self::BILINGUAL => 'Bilingual',
            self::ENGLISH => 'English',
            self::FRENCH => 'French',
            self::NOTSPECIFIED => 'Not Specified',
        ];

        $fr = [
            self::BILINGUAL => 'bilingue',
            self::ENGLISH => 'anglais',
            self::FRENCH => 'français',
            self::NOTSPECIFIED => 'non-désigné',
        ];

        // Variable variable
        return $$lang;
    }

    /**
     * Returns the list of enums in this class.
     *
     * @return string[]
     */
    public static function enums()
    {
        return [
            self::BILINGUAL,
            self::ENGLISH,
            self::FRENCH,
            self::NOTSPECIFIED,
        ];
    }
}
