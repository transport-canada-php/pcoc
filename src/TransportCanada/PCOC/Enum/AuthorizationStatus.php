<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Enum;

/**
 * Namespace for authorization response status enums.
 */
class AuthorizationStatus implements Enum
{
    /**
     * Attempt to fetch authorization / exam failed.
     *
     * @var string
     */
    const FAIL = 'Fail';

    /**
     * Success in fetching authorization.
     *
     * @var string
     */
    const OK = 'Ok';

    /**
     * Success in fetching authorization but some warnings are present.
     *
     * @var string
     */
    const OK_WARNING = 'OkWithWarnings';

    /**
     * Candidate already passed exam.
     *
     * @var string
     */
    const PASSED = 'CandidatePassed';

    /**
     * Returns a list of localized labels for each enum.
     *
     * @param string $lang Language code
     *
     * @return array
     */
    public static function labels(string $lang = 'en')
    {
        $en = [
            self::FAIL => 'Authorization failed',
            self::OK => 'Authorization succeeded',
            self::OK_WARNING => 'Authorization succeeded but with warnings',
            self::PASSED => 'Operator already passed',
        ];

        $fr = [
            self::FAIL => 'Authorization failed',
            self::OK => 'Authorization succeeded',
            self::OK_WARNING => 'Authorization succeeded but with warnings',
            self::PASSED => 'Operator already passed',
        ];

        return $$lang;
    }

    /**
     * Returns the list of enums in this class.
     *
     * @return string[]
     */
    public static function enums()
    {
        return [
            self::FAIL,
            self::OK,
            self::OK_WARNING,
            self::PASSED,
        ];
    }
}
