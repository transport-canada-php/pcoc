<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Enum;

/**
 * Namespace for state enums.
 */
class UnitedStates implements Enum
{
    /**
     * Alabama.
     *
     * @var string
     */
    const AL = Provinces::AL;

    /**
     * Alaska.
     *
     * @var string
     */
    const AK = Provinces::AK;

    /**
     * American Samoa.
     *
     * @var string
     */
    const AS = Provinces::AS;

    /**
     * Arizona.
     *
     * @var string
     */
    const AZ = Provinces::AZ;

    /**
     * Arkansas.
     *
     * @var string
     */
    const AR = Provinces::AR;

    /**
     * California.
     *
     * @var string
     */
    const CA = Provinces::CA;

    /**
     * Colorado.
     *
     * @var string
     */
    const CO = Provinces::CO;

    /**
     * Connecticut.
     *
     * @var string
     */
    const CT = Provinces::CT;

    /**
     * Delaware.
     *
     * @var string
     */
    const DE = Provinces::DE;

    /**
     * District of Columbia.
     *
     * @var string
     */
    const DC = Provinces::DC;

    /**
     * Federated States of Micronesia.
     *
     * @var string
     */
    const FM = Provinces::FM;

    /**
     * Florida.
     *
     * @var string
     */
    const FL = Provinces::FL;

    /**
     * Georgia.
     *
     * @var string
     */
    const GA = Provinces::GA;

    /**
     * Guam.
     *
     * @var string
     */
    const GU = Provinces::GU;

    /**
     * Hawaii.
     *
     * @var string
     */
    const HI = Provinces::HI;

    /**
     * Idaho.
     *
     * @var string
     */
    const ID = Provinces::ID;

    /**
     * Illinois.
     *
     * @var string
     */
    const IL = Provinces::IL;

    /**
     * Indiana.
     *
     * @var string
     */
    const IN = Provinces::IN;

    /**
     * Iowa.
     *
     * @var string
     */
    const IA = Provinces::IA;

    /**
     * Kansas.
     *
     * @var string
     */
    const KS = Provinces::KS;

    /**
     * Kentucky.
     *
     * @var string
     */
    const KY = Provinces::KY;

    /**
     * Louisiana.
     *
     * @var string
     */
    const LA = Provinces::LA;

    /**
     * Maine.
     *
     * @var string
     */
    const ME = Provinces::ME;

    /**
     * Marshall Islands.
     *
     * @var string
     */
    const MH = Provinces::MH;

    /**
     * Maryland.
     *
     * @var string
     */
    const MD = Provinces::MD;

    /**
     * Massachusetts.
     *
     * @var string
     */
    const MA = Provinces::MA;

    /**
     * Michigan.
     *
     * @var string
     */
    const MI = Provinces::MI;

    /**
     * Minnesota.
     *
     * @var string
     */
    const MN = Provinces::MN;

    /**
     * Mississippi.
     *
     * @var string
     */
    const MS = Provinces::MS;

    /**
     * Missouri.
     *
     * @var string
     */
    const MO = Provinces::MO;

    /**
     * Montana.
     *
     * @var string
     */
    const MT = Provinces::MT;

    /**
     * Nebraska.
     *
     * @var string
     */
    const NE = Provinces::NE;

    /**
     * Nevada.
     *
     * @var string
     */
    const NV = Provinces::NV;

    /**
     * New Hampshire.
     *
     * @var string
     */
    const NH = Provinces::NH;

    /**
     * New Jersey.
     *
     * @var string
     */
    const NJ = Provinces::NJ;

    /**
     * New Mexico.
     *
     * @var string
     */
    const NM = Provinces::NM;

    /**
     * New York.
     *
     * @var string
     */
    const NY = Provinces::NY;

    /**
     * North Carolina.
     *
     * @var string
     */
    const NC = Provinces::NC;

    /**
     * North Dakota.
     *
     * @var string
     */
    const ND = Provinces::ND;

    /**
     * Northern Mariana Island.
     *
     * @var string
     */
    const MP = Provinces::MP;

    /**
     * Ohio.
     *
     * @var string
     */
    const OH = Provinces::OH;

    /**
     * Oklahoma.
     *
     * @var string
     */
    const OK = Provinces::OK;

    /**
     * Oregon.
     *
     * @var string
     */
    const OR = Provinces::OR;

    /**
     * Palau.
     *
     * @var string
     */
    const PW = Provinces::PW;

    /**
     * Pennsylvania.
     *
     * @var string
     */
    const PA = Provinces::PA;

    /**
     * Puerto Rico.
     *
     * @var string
     */
    const PR = Provinces::PR;

    /**
     * Rhode Island.
     *
     * @var string
     */
    const RI = Provinces::RI;

    /**
     * South Carolina.
     *
     * @var string
     */
    const SC = Provinces::SC;

    /**
     * South Dakota.
     *
     * @var string
     */
    const SD = Provinces::SD;

    /**
     * Tennessee.
     *
     * @var string
     */
    const TN = Provinces::TN;

    /**
     * Texas.
     *
     * @var string
     */
    const TX = Provinces::TX;

    /**
     * Utah.
     *
     * @var string
     */
    const UT = Provinces::UT;

    /**
     * Vermont.
     *
     * @var string
     */
    const VT = Provinces::VT;

    /**
     * Virgin islands.
     *
     * @var string
     */
    const VI = Provinces::VI;

    /**
     * Virginia.
     *
     * @var string
     */
    const VA = Provinces::VA;

    /**
     * Washington.
     *
     * @var string
     */
    const WA = Provinces::WA;

    /**
     * West Virginia.
     *
     * @var string
     */
    const WV = Provinces::WV;

    /**
     * Wisconsin.
     *
     * @var string
     */
    const WI = Provinces::WI;

    /**
     * Wyoming.
     *
     * @var string
     */
    const WY = Provinces::WY;

    /**
     * Returns a list of localized labels for each enum.
     *
     * @param string $lang Language code
     *
     * @return array
     */
    public static function labels(string $lang = 'en')
    {
        // Define English translations
        $en = [
            self::AL => 'Alabama',
            self::AK => 'Alaska',
            self::AS => 'American Samoa',
            self::AZ => 'Arizona',
            self::AR => 'Arkansas',
            self::CA => 'California',
            self::CO => 'Colorado',
            self::CT => 'Connecticut',
            self::DE => 'Delaware',
            self::DC => 'District of Columbia',
            self::FM => 'Federated States of Micronesia',
            self::FL => 'Florida',
            self::GA => 'Georgia',
            self::GU => 'Guam',
            self::HI => 'Hawaii',
            self::ID => 'Idaho',
            self::IL => 'Illinois',
            self::IN => 'Indiana',
            self::IA => 'Iowa',
            self::KS => 'Kansas',
            self::KY => 'Kentucky',
            self::LA => 'Louisiana',
            self::ME => 'Maine',
            self::MH => 'Marshall Islands',
            self::MD => 'Maryland',
            self::MA => 'Massachusetts',
            self::MI => 'Michigan',
            self::MN => 'Minnesota',
            self::MS => 'Mississippi',
            self::MO => 'Missouri',
            self::MT => 'Montana',
            self::NE => 'Nebraska',
            self::NV => 'Nevada',
            self::NH => 'New Hampshire',
            self::NJ => 'New Jersey',
            self::NM => 'New Mexico',
            self::NY => 'New York',
            self::NC => 'North Carolina',
            self::ND => 'North Dakota',
            self::MP => 'Northern Mariana Island',
            self::OH => 'Ohio',
            self::OK => 'Oklahoma',
            self::OR => 'Oregon',
            self::PW => 'Palau',
            self::PA => 'Pennsylvania',
            self::PR => 'Puerto Rico',
            self::RI => 'Rhode Island',
            self::SC => 'South Carolina',
            self::SD => 'South Dakota',
            self::TN => 'Tennessee',
            self::TX => 'Texas',
            self::UT => 'Utah',
            self::VT => 'Vermont',
            self::VI => 'Virgin islands',
            self::VA => 'Virginia',
            self::WA => 'Washington',
            self::WV => 'West Virginia',
            self::WI => 'Wisconsin',
            self::WY => 'Wyoming',
        ];

        // Define French translations
        $fr = [
            self::AL => 'Alabama',
            self::AK => 'Alaska',
            self::AS => 'American Samoa',
            self::AZ => 'Arizona',
            self::AR => 'Arkansas',
            self::CA => 'California',
            self::CO => 'Colorado',
            self::CT => 'Connecticut',
            self::DE => 'Delaware',
            self::DC => 'District of Columbia',
            self::FM => 'Federated States of Micronesia',
            self::FL => 'Florida',
            self::GA => 'Georgia',
            self::GU => 'Guam',
            self::HI => 'Hawaii',
            self::ID => 'Idaho',
            self::IL => 'Illinois',
            self::IN => 'Indiana',
            self::IA => 'Iowa',
            self::KS => 'Kansas',
            self::KY => 'Kentucky',
            self::LA => 'Louisiana',
            self::ME => 'Maine',
            self::MH => 'Marshall Islands',
            self::MD => 'Maryland',
            self::MA => 'Massachusetts',
            self::MI => 'Michigan',
            self::MN => 'Minnesota',
            self::MS => 'Mississippi',
            self::MO => 'Missouri',
            self::MT => 'Montana',
            self::NE => 'Nebraska',
            self::NV => 'Nevada',
            self::NH => 'New Hampshire',
            self::NJ => 'New Jersey',
            self::NM => 'New Mexico',
            self::NY => 'New York',
            self::NC => 'North Carolina',
            self::ND => 'North Dakota',
            self::MP => 'Northern Mariana Island',
            self::OH => 'Ohio',
            self::OK => 'Oklahoma',
            self::OR => 'Oregon',
            self::PW => 'Palau',
            self::PA => 'Pennsylvania',
            self::PR => 'Puerto Rico',
            self::RI => 'Rhode Island',
            self::SC => 'South Carolina',
            self::SD => 'South Dakota',
            self::TN => 'Tennessee',
            self::TX => 'Texas',
            self::UT => 'Utah',
            self::VT => 'Vermont',
            self::VI => 'Virgin islands',
            self::VA => 'Virginia',
            self::WA => 'Washington',
            self::WV => 'West Virginia',
            self::WI => 'Wisconsin',
            self::WY => 'Wyoming',
        ];

        // Using a variable variable, we can access either the French or English
        // translations just by calling the function with either declared variable
        // names (e.g.) Enum\Provinces::labels('fr');
        return $$lang;
    }

    /**
     * Returns the list of enums in this class.
     *
     * @return string[]
     */
    public static function enums()
    {
        return [
            self::AL,
            self::AK,
            self::AS,
            self::AZ,
            self::AR,
            self::CA,
            self::CO,
            self::CT,
            self::DE,
            self::DC,
            self::FM,
            self::FL,
            self::GA,
            self::GU,
            self::HI,
            self::ID,
            self::IL,
            self::IN,
            self::IA,
            self::KS,
            self::KY,
            self::LA,
            self::ME,
            self::MH,
            self::MD,
            self::MA,
            self::MI,
            self::MN,
            self::MS,
            self::MO,
            self::MT,
            self::NE,
            self::NV,
            self::NH,
            self::NJ,
            self::NM,
            self::NY,
            self::NC,
            self::ND,
            self::MP,
            self::OH,
            self::OK,
            self::OR,
            self::PW,
            self::PA,
            self::PR,
            self::RI,
            self::SC,
            self::SD,
            self::TN,
            self::TX,
            self::UT,
            self::VT,
            self::VI,
            self::VA,
            self::WA,
            self::WV,
            self::WI,
            self::WY,
        ];
    }
}
