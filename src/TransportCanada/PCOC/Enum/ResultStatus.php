<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Enum;

/**
 * Namespace for result status enums.
 */
class ResultStatus implements Enum
{
    /**
     * Candidate failed the test.
     *
     * @var string
     */
    const FAIL = 'Fail';

    /**
     * Candidate has yet to complete the test.
     *
     * @var string
     */
    const INCOMPLETE = 'Incomplete';

    /**
     * Candidate has yet to start the test.
     *
     * @var string
     */
    const NOT_STARTED = 'NotStarted';

    /**
     * Candidate passed the test.
     *
     * @var string
     */
    const PASS = 'Pass';

    /**
     * Test status is unknown.
     *
     * @var string
     */
    const UNKNOWN = 'Unknown';

    /**
     * Returns a list of localized labels for each enum.
     *
     * @param string $lang Language code
     *
     * @return array
     */
    public static function labels(string $lang = 'en')
    {
        $en = [
            self::FAIL => 'Request failed',
            self::INCOMPLETE => 'The associated exam has not been completed',
            self::NOT_STARTED => 'The associated exam has not been started',
            self::PASS => 'The associated exam has already been passed',
        ];

        $fr = [
            self::FAIL => 'La demande à échoué',
            self::INCOMPLETE => 'L\'examain associé na pas été completé',
            self::NOT_STARTED => 'L\'examain associé na pas été commencé',
            self::PASS => 'L\'examain associé as déja été passé',
        ];

        return $$lang;
    }

    /**
     * Returns the list of enums in this class.
     *
     * @return string[]
     */
    public static function enums()
    {
        return [
            self::FAIL,
            self::INCOMPLETE,
            self::NOT_STARTED,
            self::PASS,
        ];
    }
}
