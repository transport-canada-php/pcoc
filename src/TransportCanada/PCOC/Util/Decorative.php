<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Util;

/**
 * Convert current object into one specified by static::$decorate class path.
 *
 * This function is recursive: if a property has a `decorate` method then it'll
 * be invoked and will replace the original value.
 *
 * <samp>
 * <?php
 * class Original {
 *     use Decorate;
 *     protected static $decorate = Converted::class;
 *     public $null = 'inherited', $property = 'override', $original = true;
 * }
 *
 * class Converted {
 *     public $null, $property = 'default', $converted = true;
 * }
 *
 * $original = new Original;
 * $converted = new Converted;
 * $decorated = $original->decorate();
 * ?>
 *
 * $original = object(Original) {
 *     ['null'] => 'inherit'
 *     ['property'] => 'override'
 *     ['original'] => true
 * }
 *
 * $converted = object(Converted) {
 *     ['null'] => null
 *     ['property'] => 'default'
 *     ['converted'] => true
 * }
 *
 * $decorated = object(Converted) {
 *     ['null'] => 'inherit'
 *     ['property'] => 'override'
 *     ['converted'] => true
 * }
 * </samp>
 */
trait Decorative
{
    /**
     * Converts the current object into an instance of the decorate class.
     *
     * @return object
     */
    public function decorate()
    {
        $decorate = new static::$decorate();
        $keys = array_keys(get_object_vars($decorate));

        foreach ($keys as $key) {
            $value = $this->$key;
            // Decorators can be nested
            $callable = \is_object($value) && method_exists($value, 'decorate');
            $decorate->$key = $callable ? \call_user_func([$value, 'decorate']) : $value;
        }

        return $decorate;
    }
}
