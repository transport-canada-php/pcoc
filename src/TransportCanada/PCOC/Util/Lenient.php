<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Util;

/**
 * Bundled traits for easy object handling.
 *
 * @property array $insensitive {@see TransportCanada\PCOC\Util\Insensitive::$insensitive}
 *
 * @method object decorate()                                           {@see \TransportCanada\PCOC\Util\Decorative::decorate()}
 * @method string insensitive(string $name)                            {@see \TransportCanada\PCOC\Util\Insensitive::insensitive()}
 * @method mixed  __get(string $name)                                  {@see \TransportCanada\PCOC\Util\Insensitive::__get()}
 * @method null   __set(string $name, $value)                          {@see \TransportCanada\PCOC\Util\Insensitive::__set()}
 * @method bool   __isset(string $name)                                {@see \TransportCanada\PCOC\Util\Insensitive::__isset()}
 * @method null   __unset(string $name)                                {@see \TransportCanada\PCOC\Util\Insensitive::__unset()}
 * @method null   __construct($with = [])                              {@see \TransportCanada\PCOC\Util\Objective::__construct()}
 * @method null   hydrate(object $object, $data, bool $create = false) {@see \TransportCanada\PCOC\Util\Objective::hydrate()}
 * @method null   fill($with, bool $create = false)                    {@see \TransportCanada\PCOC\Util\Objective::fill()}
 */
trait Lenient
{
    use Decorative;
    use Insensitive;
    use Objective;
}
